const express = require("express");
const path = require("path");

const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.static(path.join(__dirname, "../public/js")))

app.set("view engine", "ejs");

app.get("/", (request, response) => {
    response.render("homePage")
});

app.get("/problem1", (request, response) => {
    response.render("main", { title: "Matches per year", src: "../problem1.js" })
});

app.get("/problem2", (request, response) => {
    response.render("main", { title: "Matches won per team per year", src: "../problem2.js" })
});

app.get("/problem3", (request, response) => {
    response.render("main", { title: "Extra runs per team in the year 2016", src: "../problem3.js" })
});

app.get("/problem4", (request, response) => {
    response.render("main", { title: "Top economical bowlers", src: "../problem4.js" })
});

app.get("/problem5", (request, response) => {
    response.render("main", { title: "Teams won toss and match", src: "../problem5.js" })
});

app.get("/problem6", (request, response) => {
    response.render("main", { title: "Player of the match", src: "../problem6.js" })
});

app.get("/problem7", (request, response) => {
    response.render("main", { title: "Strike rate of a batsman", src: "../problem7.js" })
});

app.get("/problem8", (request, response) => {
    response.render("main", { title: "Player dismissed", src: "../problem8.js" })
});

app.get("/problem9", (request, response) => {
    response.render("main", { title: "Bowler with best economy in super over", src: "../problem9.js" })
});

app.all("*", (request, response) => {
    response.status(404).json({
        error: "Not found"
    }).end();    
});

app.use((error, request, response, next) => {
    console.error(error.stack)
    response.status(500).send('Something broke!')
  })

app.listen(PORT, () => console.log(`Server running on ${PORT}`));