
const highCharts = (iplData) => {

    const keyArray = Object.keys(iplData)
    let valueArray = Object.values(iplData)

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches won per year'
        },
        subtitle: {
            text: 'Source: IPL data'
        },
        xAxis: {
            categories: keyArray,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'matches'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '2008',
            data: [49.9, 51.5, 56.4, 67.2, 77.0, 76.0, 35.6, 48.5, 108.4,
                45.1, 75.6, 54.4]

        }, {
            name: '2009',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5,
                106.6, 92.3]

        }, {
            name: '2010',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3,
                51.2]

        }, {
            name: '2011',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8,
                51.1]

        }, {
            name: '2012',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3,
                51.2]

        }, {
            name: '2013',
            data: [38.9, 28.8, 39.3, 21.4, 67.0, 68.3, 49.0, 29.6, 72.4, 85.2, 79.3,
                71.2]

        }, {
            name: '2014',
            data: [68.9, 58.8, 59.3, 61.4, 67.0, 68.3, 79.0, 79.6, 72.4, 85.2, 79.3,
                71.2]

        }, {
            name: '2015',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3,
                51.2]

        }, {
            name: '2016',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3,
                51.2]

        }, {
            name: '2017',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3,
                51.2]

        },]
    });

}

const problem2 = () => {
    fetch("./2-matches-won-per-team-per-year.json")
        .then(response => response.json())
        .then(response => highCharts(response))
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function () {
    problem2();
});