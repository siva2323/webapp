const matchesPlayed = (iplData) => {

    const keyArray = Object.keys(iplData);
    const valueArray = Object.values(iplData);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Match Data'
        },
        subtitle: {
            text: 'Source: <a href="https://worldpopulationreview.com/world-cities" target="_blank">IPL data </a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total result'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ' <b>{point.y:.f}</b>'
        },
        series: [{
            name: 'Population',
            data: (Object.entries(iplData)),
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.f}',
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}

const problem5 = () => {
    fetch("./5-teamsWonTossAndMatch.json")
        .then(response => response.json())
        .then(response => matchesPlayed(response))
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function () {
    problem5();
});