
const bestEconomyBowler = (iplData) => {

    const keyArray = Object.keys(iplData);
    const valueArray = Object.values(iplData);

    const chart = Highcharts.chart('container', {
        title: {
            text: 'Bowler with best economy',
            align: 'left'
        },
        colors: [
            '#4caefe',
        ],
        xAxis: {
            categories: [iplData]
        },
        series: [{
            type: 'column',
            name: 'Player',
            borderRadius: 3,
            colorByPoint: true,
            data: [0.75],
            showInLegend: false
        }]
    });


}

const problem9 = () => {
    fetch("./9-bowler-with-best-economy-in-super-over.json")
        .then(response => response.json())
        .then(response => bestEconomyBowler(response))
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function () {
    problem9();
});