
const strikeRate = (iplData) => {

    iplData = iplData.map(item => item.split(":"))
    iplData = Object.fromEntries(iplData)
    const keyArray = Object.keys(iplData);
    let valueArray = Object.values(iplData);
    valueArray = valueArray.map(item => Number(item))


    const chart = Highcharts.chart('container', {
        title: {
            text: 'Strike rate',
            align: 'left'
        },

        colors: [
            '#4caefe',
            '#3fbdf3',
            '#35c3e8',
            '#2bc9dc',
            '#20cfe1',
            '#16d4e6',
            '#0dd9db',
            '#03dfd0',
            '#00e4c5',
            '#00e9ba',
            '#00eeaf',
            '#23e274'
        ],
        xAxis: {
            categories: keyArray
        },
        series: [{
            type: 'column',
            name: 'Unemployed',
            borderRadius: 5,
            colorByPoint: true,
            data: valueArray,
            showInLegend: false
        }]
    });


}


const problem7 = () => {
    fetch("./7-strike-rate-of-a-batsman.json")
        .then(response => response.json())
        .then(response => strikeRate(response))
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function () {
    problem7();
});