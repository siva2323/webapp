const playerDismissed = (iplData) => {

    const keyArray = Object.keys(iplData);
    const valueArray = Object.values(iplData);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            align: 'left',
            text: 'Player Dismissed'
        },
        subtitle: {
            align: 'left',
            text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total times dismissed'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> of total<br/>'
        },

        series: [
            {
                name: 'Player',
                colorByPoint: true,
                data: [valueArray, keyArray]
            }
        ],
        drilldown: {
            breadcrumbs: {
                position: {
                    align: 'right'
                }
            },
        }
    });
}


const problem8 = () => {
    fetch("./8-player-dismissed.json")
        .then(response => response.json())
        .then(response => playerDismissed(response))
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function () {
    problem8();
});